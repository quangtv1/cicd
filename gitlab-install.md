sudo timedatectl set-timezone Asia/Ho_Chi_Minh

# Lab 1
https://www.snel.com/support/how-to-create-your-own-gitlab-server-on-ubuntu-18-04/

$ sudo apt update
$ sudo apt install ca-certificates curl openssh-server ufw apt-transport-https -y
$ sudo apt install postfix -y
$ sudo ufw allow openssh
$ sudo ufw enable
$ sudo ufw allow http
$ sudo ufw allow https
$ sudo ufw allow Postfix
$ sudo ufw status

$ curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
$ sudo EXTERNAL_URL="https://gitlab.nuce.edu.vn" apt-get install gitlab-ee

# Cài đặt cấu hình email tự động postfix qua gmail
$ sudo apt install mailutils
$ sudo postconf -e 'relayhost = [smtp.gmail.com]:587'
$ sudo postconf -e 'smtp_tls_security_level = may'
$ sudo postconf -e 'myhostname = gitlab.nuce.edu.vn'
$ sudo service postfix restart

$ sudo vi /etc/postfix/sasl/sasl_passwd
    [smtp.gmail.com]:587 gitlab@nuce.edu.vn:Nuce@1234
$ sudo postmap /etc/postfix/sasl/sasl_passwd
$ sudo chown root:root /etc/postfix/sasl/sasl_passwd /etc/postfix/sasl/sasl_passwd.db
$ sudo chmod 0600 /etc/postfix/sasl/sasl_passwd /etc/postfix/sasl/sasl_passwd.db

$ sudo vi /etc/postfix/main.cf
    # Enable SASL authentication
    smtp_sasl_auth_enable = yes
    # Disallow methods that allow anonymous authentication
    smtp_sasl_security_options = noanonymous
    # Location of sasl_passwd
    smtp_sasl_password_maps = hash:/etc/postfix/sasl/sasl_passwd
    # Enable STARTTLS encryption
    smtp_tls_security_level = encrypt
    # Location of CA certificates
    smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt

$ sudo systemctl restart postfix
$ echo "This email confirms that Postfix is working" | mail -s "Testing Postfix" quangtv@nuce.edu.vn

# Tạo ssh key và add trên SRV
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDIiDhLMfmtFbduLGUtnplEGoT1VHnvowYOSDSxirAQEnUVwGiZh3GL/F8E9+KCXdc3ERyG0M+P4V0+MBO1SqFnWbqs+Zu9M/FITIbx4dqnPirQoBIV9QjltvgoKtqNKIR8NVSST1+/aJJNTB+f1DEjotjVFPWmj18vAG2oaunHgWy54x3HriPD6Whxuiamyi1CEmZiEMrShP+ROA5inWg1lExXyqh+1j2XTM4UWyXWLJX+1J8pKx7oLk6IjaKQzwn25ZNL/IAwMpBNwTuH6wHK9/b8e5n4Pm4kgAwrGR2eLSHDiZGXgkBxXVmEE/i7/dHiKzjICTm4UfKn73FZg+TP0cXfQjyfUICJHurRh5lA3Gwf2R+s7shLuz6e2IZAoSEXpsWi4aRHwBTUG/309BRWeVUYT0hBZvM8sDfIwWdwa9GPc4CnM3Nk5fpmb5rG6uy4d8GQDQ5fDwC3nupn2DrjcNEcn8MxBfK/KutdtzeVx2tviYH+H8lpq4zjk/JHTl8= quangtv@WS03

# User access token
n-gxtjr_NohA1N7eMcb5

# Tim kiem
find . -type f -exec grep -H 'nuce.edu.vn' {} \;
find . -name '*.conf' -exec grep -i 'nuce.edu.vn' {} \; -print
find / -name 'admin_cdwsatreps.sql' 
du -shc /home /var/log



sudo hostnamectl set-hostname gitlab.nuce.edu.vn --static
sudo gitlab-ctl reconfigure
sudo gitlab-ctl restart
sudo gitlab-ctl hup nginx
sudo gitlab-rake gitlab:check