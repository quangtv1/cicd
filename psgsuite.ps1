# Chạy Powersuite dưới quyền administrator

# Update NuGet
Install-PackageProvider -Name NuGet -Force

# Tải PSGSuite
Install-Module Configuration -Scope AllUsers 
Install-Module -Name PSGSuite -RequiredVersion 2.36.4
# Install-Module -Name PSGSuite -Scope AllUsers -Force -AllowClobber

#Import PSGSuite Module
Import-Module PSGSuite -Scope AllUsers

# Paste vào powershell
$P12KeyPath = 'C:\Users\WS03\Desktop\Lung tung\psgsuite-ws03-e70b66e44393.p12'
$AppEmail = 'psgsuite-ws03@psgsuite-ws03.iam.gserviceaccount.com'
$AdminEmail = 'quangtv@nuce.edu.vn'
$CustomerID = 'C01h6ywoi'
$Domain = 'nuce.edu.vn'
$Preference = 'CustomerID'
$ServiceAccountClientID = '103626773064874273059'
# Set cấu hình
Set-PSGSuiteConfig -ConfigName quangtvConfig -SetAsDefaultConfig -P12KeyPath $P12KeyPath -AppEmail $AppEmail -AdminEmail $AdminEmail -CustomerID $CustomerID -Domain $Domain -Preference $Preference -ServiceAccountClientID $ServiceAccountClientID
###############################################


# Các câu lệnh hay dùng
Update-GSUser [-User] <String[]> [-PrimaryEmail <String>] [-FullName <String>] [-Password <SecureString>]
 [-ChangePasswordAtNextLogin] [-OrgUnitPath <String>] [-Suspended] [-Addresses <UserAddress[]>]
 [-Emails <UserEmail[]>] [-ExternalIds <UserExternalId[]>] [-Ims <UserIm[]>] [-Locations <UserLocation[]>]
 [-Organizations <UserOrganization[]>] [-Relations <UserRelation[]>] [-RecoveryEmail <String>]
 [-RecoveryPhone <String>] [-Phones <UserPhone[]>] [-IncludeInGlobalAddressList] [-IpWhitelisted] [-IsAdmin]
 [-Archived] [-CustomSchemas <Hashtable>] [-WhatIf] [-Confirm] [<CommonParameters>]

# Lấy thông tin người dùng
Get-GSUser dat46862@nuce.edu.vn
Get-GSUser lichcongtac@nuce.edu.vn

# Cập nhật số điện thoại và email phục hồi:
Update-GSUser -User lichcongtac@nuce.edu.vn -RecoveryPhone +84979824615 -Confirm:$false
Update-GSUser -User lichcongtac@nuce.edu.vn -RecoveryEmail tranvqhn@yahoo.com -Confirm:$false



Update-GSUser -User anhtth@nuce.edu.vn -Password (ConvertTo-SecureString "22081987@anh" -AsPlainText -Force) -Confirm:$false
# Công thức Google sheet
=CONCATENATE("Update-GSUser -User ", C9, " -Password (ConvertTo-SecureString  ", char(34),J9, char(34), " -AsPlainText -Force) -ChangePasswordAtNextLogin:$false -Confirm:$false")
# Reset password a user
Update-GSUser -User anhtth@nuce.edu.vn -Password (ConvertTo-SecureString  "22081987@anh" -AsPlainText -Force) -ChangePasswordAtNextLogin:$false -Confirm:$false
Update-GSUser -User bacdp@nuce.edu.vn -Password (ConvertTo-SecureString  "12091980@bac" -AsPlainText -Force) -ChangePasswordAtNextLogin:$false -Confirm:$false
Update-GSUser -User chinhvt@nuce.edu.vn -Password (ConvertTo-SecureString  "25021979@chinh" -AsPlainText -Force) -ChangePasswordAtNextLogin:$false -Confirm:$false
Update-GSUser -User diepbn@nuce.edu.vn -Password (ConvertTo-SecureString  "02051980@diep" -AsPlainText -Force) -ChangePasswordAtNextLogin:$false -Confirm:$false
Update-GSUser -User dungln2@nuce.edu.vn -Password (ConvertTo-SecureString  "05091971@dung" -AsPlainText -Force) -ChangePasswordAtNextLogin:$false -Confirm:$false
Update-GSUser -User dungps@nuce.edu.vn -Password (ConvertTo-SecureString  "06071986@dung" -AsPlainText -Force) -ChangePasswordAtNextLogin:$false -Confirm:$false
Update-GSUser -User hienltt@nuce.edu.vn -Password (ConvertTo-SecureString  "17031974@hien" -AsPlainText -Force) -ChangePasswordAtNextLogin:$false -Confirm:$false
