# Cài zsh 5.1, oh-my-zsh: zsh-autosuggestions, zsh-syntax-highlighting, powerlevel10k
cd /
wget http://mirror.ghettoforge.org/distributions/gf/el/7/plus/x86_64/zsh-5.1-1.gf.el7.x86_64.rpm
rpm -Uhv  zsh-5.1-1.gf.el7.x86_64.rpm
yum --enablerepo=gf-plus install zsh
yum install zsh
chsh -s /bin/zsh root
exec zsh
echo $SHELL

# Cằi đặt oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
# Cài đặt plugin và theme
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
# Manual theme
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k

# git clone https://github.com/ryanoasis/nerd-fonts
# sudo ./install.sh

vi ~/.zshrc
    ZSH_THEME="powerlevel10k/powerlevel10k"
    plugins=(git zsh-autosuggestions zsh-syntax-highlighting)
    source /root/.oh-my-zsh/custom/themes/powerlevel10k/powerlevel10k.zsh-theme
    # To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
    [[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
source ~/.zshrc

p10k configure
