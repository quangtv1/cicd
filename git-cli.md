# Các câu lệnh git thường dùng
ssh-keygen

# Check remote đang dùng
git remote -v
git remote rm origin
git global --list

git log
git show id-commit
git diff
git status -s


git clone git@gitlab.com:quangtv1/hello-world.git
git clone https://gitlab.com/quangtv1/hello-world.git
git pull

git checkout
git branch
git checkout -b test_branch # Chuyển sang branch
git branch -D test_branch # Xoá branch, phải chuyến sang Branch khác trước
git branch -m old_branch new_support # Đổi tên nhánh
git merge origin/branch-dev # Chuyển về branch master trước khi megre

git remote add origin git@gitlab.com:quangtv1/hello-world.git
git push -u origin master