# Cài đặt zabbit trên PostgreSQL 11, Centos 7
###################################################

# Download PGDG for PostgreSQL 11, e.g. for RHEL 7:
sudo yum install -y https://download.postgresql.org/pub/repos/yum/11/redhat/rhel-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm

# Add our repo
sudo tee /etc/yum.repos.d/timescale_timescaledb.repo <<EOL
[timescale_timescaledb]
name=timescale_timescaledb
baseurl=https://packagecloud.io/timescale/timescaledb/el/7/\$basearch
repo_gpgcheck=1
gpgcheck=0
enabled=1
gpgkey=https://packagecloud.io/timescale/timescaledb/gpgkey
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300
EOL
sudo yum update -y

# Now install appropriate package for PG version
sudo yum install -y timescaledb-postgresql-9.6

nano /var/lib/pgsql/9.6/data/postgresql.conf
    # Modify postgresql.conf to uncomment this line and add required libraries.
    # For example:
    shared_preload_libraries = 'timescaledb'


# Install Zabbix repository
rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm
yum clean all
yum install zabbix-server-pgsql zabbix-agent
yum install centos-release-scl
nano /etc/yum.repos.d/zabbix.repo


yum install zabbix-web-pgsql-scl zabbix-apache-conf-scl
sudo -u postgres createuser --pwprompt zabbix
sudo -u postgres createdb -O zabbix zabbix
zcat /usr/share/doc/zabbix-server-pgsql*/create.sql.gz | sudo -u zabbix psql zabbix
nano /etc/zabbix/zabbix_server.conf
# DBPassword=password

sudo yum search php72 | egrep 'fpm|gd|pgsql|memcache'
yum install centos-release-scl-rh


yum install php72-php-fpm php72-php-gd php72-php-json php72-php-mbstring php72-php-mysqlnd php72-php-xml php72-php-xmlrpc php72-php-opcache rh-php72 rh-php72-php rh-php72-php-gd rh-php72-php-mbstring rh-php72-php-intl rh-php72-php-pecl-apcu rh-php72-php-fpm rh-php72-php-pgsql rh-php72-php-ldap

ln -s /opt/rh/rh-php72/root/usr/bin/php /usr/bin/php

sudo scl enable rh-php72 bash

systemctl restart zabbix-server zabbix-agent httpd rh-php72-php-fpm

mysql -u root -p -h localhost dr7 < /home/quangtv/backup/nuce.edu.vn.bak-2020-12-14.sql

# tb_node
SELECT `nid`, `vid`, `type`, `language`, `title`, `uid`, `status`, `created`, `changed`, `comment`, `promote`, `sticky`, `tnid`, `translate` FROM `tb_node` WHERE 1

# tb_field_data_body
SELECT `entity_type`, `bundle`, `deleted`, `entity_id`, `revision_id`, `language`, `delta`, `body_value`, `body_summary`, `body_format` FROM `tb_field_data_body` WHERE 1

# tb_node_type
SELECT `type`, `name`, `base`, `module`, `description`, `help`, `has_title`, `title_label`, `custom`, `modified`, `locked`, `disabled`, `orig_type` FROM `tb_node_type` WHERE 1

# tb_url_alias
SELECT `pid`, `source`, `alias`, `language` FROM `tb_url_alias` WHERE 1

GRANT ALL ON *.* to 'root@'192.168.206.2' IDENTIFIED BY 'password';