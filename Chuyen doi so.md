Cấu hình SRV AI - A Thắng

- Processor: Intel Xeon Gold 6226 2.7G, 12C/24T, 10.4GT/s, 19.25M Cache, Turbo, HT (125W) DDR4-2933
- Additional Processor: Intel Xeon Gold 6226 2.7G, 12C/24T, 10.4GT/s, 19.25M Cache, Turbo, HT (125W) DDR4-2933
- Memory Capacity: 6 x 32GB RDIMM, 2933MT/s, Dual Rank.
- Hard Drives :2x480GB SSD SATA Read Intensive 6Gbps 512 2.5in Hot-plug AG Drive, 1 DWPD, 876 TBW
- Hard Drives	: 10 x 2.4TB 10K RPM SAS 12Gbps 512e 2.5in Hot-plug Hard Drive
- GPU/FPGA/Acceleration Cards: 3x NVIDIA® Tesla™ T4 16GB, Passive, Single Wide, Full Height GPU
- Lan, wifi: On Board
- Power Supply: Dual, Hot-plug, Redundant Power Supply (1+1), 1600W, 250 Volt Power Cord Required for Use


Danh mục đầu tư hạng mục phần cứng - Dự án chuyển đổi số
########################################################
STT	TÊN THIẾT BỊ		ĐVT	SL	MODEL - HÃNG SẢN XUẤT - XUẤT XỨ	Ghi chú 
III	HỆ THỐNG MÁY CHỦ, HỆ ĐIỀU HÀNH MÁY CHỦ, CSDL VÀ LƯU TRỮ DỮ LIỆU					
1	Máy chủ ứng dụng hệ thống		Chiếc	2	"Dell PowerEdge R940
Hãng sản xuất: Dell - Mỹ"	Chạy ứng dụng hệ thống
2	Máy chủ  Cơ sở dữ liệu		Chiếc	2	"Dell PowerEdge R940
Hãng sản xuất: Dell - Mỹ"	Chạy ứng dụng Database
3	Máy chủ Ứng dụng AI		Chiếc	2	"PowerEdge C4140 Server
Hãng sản xuất: Dell - Mỹ"	Chạy ứng dụng Artificial Intelligence and MC
4	Hệ thống lưu trữ SAN		Chiếc	1	"Mã sản phẩm: ME4024
Hãng sản xuất: Dell - Mỹ"	"Ghi chú: 
- Ổ cứng 20 ổ x 2,4TB Sas 10krpm
- Ổ cứng 10 ổ x 1.2TB Sas 10krpm"
5	 SAN Switch		Chiếc	2	" Mã sản phẩm: 300 Brocade
Hãng sản xuất: Dell - Mỹ"	Ghi chú: 8Gb FC Switch 24 x 8G SFP
	Licensse					
6	Hệ điều hành máy chủ: Window 2016 Server Std		Licensse	6	"Hãng sản xuất: Microsoft
Xuất xứ: Singapore"	
7	Hệ quản trị CSDL: SQLSvrStd 2017 SNGL OLP NL		Licensse	2	"Hãng sản xuất: Microsoft
Xuất xứ: Singapore"	
8	Licence ảo hoá máy chủ Vmware		Licensse	4	"Model: VMware vSphere Enterprise Plus 6.5
Hãng sản xuất: Vmware
Xuất xứ: Mỹ"	Thêm, rất quan trọng
9	License AP WLC Wireless Controller 5520		Licensse	50	"Hãng sản xuất: Cisco
Xuất xứ: Mỹ"	Đã có nhưng thiếu khi bổ sung thêm 50 AP
IV	HỆ THỐNG THIẾT BỊ MẠNG, BẢO MẬT					
1	Thiết bị cân bằng tải		Chiếc	2	 FortiADC-1200F - Hãng sản xuất Fortinet - Trung Quốc	
2	Thiết bị chuyển mạch phân phối		Chiếc	6	"Cisco Catalyst 9300 Series Switches:
- Mã sản phẩm : C9300-24T-A
- Hãng sản xuất: Cisco  Mỹ"	"- 03 cho hệ thống server.
- 03 cho hệ thống các toà 315H1, H3, Thí nghiệm."
3	Thiết bị chuyển mạch truy cập		Chiếc	15	"Cisco Catalyst 2960-S Series
- Mã sản phẩm: WS-C2960X-48TS-L
- Hãng sản xuất: Cisco  Mỹ"	"- Bổ sung cho Giảng đường H1, H2, H3 (chưa có)
- Thay thế cho nhà Thí Nghiệm đang dùng từ 2006"
4	Tường lửa trung tâm		Chiếc	2	"Cisco Firepower 4100 Series: 
- Mã sản phẩm : Cisco FPR4112-NGFW-K9
- Hãng sản xuất: Cisco  Mỹ"	Bảo vệ trực tiếp cho hệ thống Server, chạy HA
5	Thiết bị xác thực hệ thống		Chiếc	1	"Secure Network Server 3655
- Hãng sản xuất: Cisco  Mỹ"	Xác thực bảo vệ mọi truy cập từ mạng LAN
6	Thiết bị wifi Indoor cho phòng học		chiếc	50	" Cisco Aironet AP1840I Series: 
- Mã sản phẩm: AIR-AP1840I-E-K9
 "	Cần mua thêm license cho Wifi Controller 5520
V	PHẦN MỀM PHỤC VỤ CHUYỂN ĐỐI SỐ NHÀ TRƯỜNG					
1	Phần mềm		Bộ	1		
########################################################

III	HỆ THỐNG MÁY CHỦ, HỆ ĐIỀU HÀNH MÁY CHỦ, CSDL VÀ LƯU TRỮ DỮ LIỆU					
1	Máy chủ ứng dụng hệ thống		Chiếc	2	"Dell PowerEdge R940
Hãng sản xuất: Dell - Mỹ"	Chạy ứng dụng hệ thống
	PowerEdge R940 Motherboard	2x Intel Xeon Gold 6252 2.1G, 24C/48T, 10.4GT/s, 35.75M Cache, Turbo, HT (150W) DDR4-2933				
		2x Additional Processor Selected				
		4 CPU Heatsink, 3UPI				
		iDRAC Group Manager, Enabled 				
		iDRAC,Factory Generated Password				
		Ex-factory Delivery				
		2.5" Chassis with up to 24 Hard Drives 				
		PowerEdge R940 Shipping - APCC/TW V2 				
		14G OpenManage placemat				
		PowerEdge R940 Shipping Material				
		Dell/EMC LCD Bezel for PowerEdge R940				
		Dell EMC Luggage Tag				
		Quick Sync 2 (At-the-box mgmt) 				
		3200MT/s RDIMMs				
		Performance Optimized 				
		4x64GB RDIMM, 3200MT/s, Dual Rank				
		iDRAC9,Enterprise				
		2x 800GB SSD SAS Mix Use 12Gbps 512e 2.5in Hot-plug AG Drive, 3 DWPD, 4380TBW				
		1.2TB 10K RPM SAS 12Gbps 512n 2.5in Hot-plug Hard Drive				
		PERC H740P RAID Controller, 8GB NV Cache, Adapter, Full Height				
		Dell EMC PowerEdge SFP+ SR Optic 10GbE 850nm 				
		Performance BIOS Settings				
		Dual, Hot-plug, Redundant Power Supply (1+1), 1600W, 250 Volt Power Cord Required for Use				
		Jumper Cord - C13/C14, 2M, 250V, 10A (EU, TW, APCC countries except ANZ)				
		PowerEdge R940 CE Marking for over 1300W PSU, APCC/Taiwan 				
		Broadcom 5720 Quad Port 1GbE BASE-T, rNDC				
		Intel X710 Quad Port 10GbE Direct Attach SFP+ Adapter, PCIe Full Height				
		Emulex LPE 31002 Dual Port 16Gb Fibre Channel HBA, PCIe Full Height				
		Cable Management Arm				
		Unconfigured RAID				
		UEFI BIOS Boot Mode with GPT Partition 				
		ProSupport Plus and Next Business Day Onsite Service-ACDTS Initial, 36Month(s)				
		Mod Specs Info (SADMG)				
		EX-HUB from APCC Penang(DDD) 				
2	Máy chủ  Cơ sở dữ liệu		Chiếc	2	"Dell PowerEdge R940
Hãng sản xuất: Dell - Mỹ"	
	PowerEdge R940 Motherboard	2x Intel Xeon Gold 6252 2.1G, 24C/48T, 10.4GT/s, 35.75M Cache, Turbo, HT (150W) DDR4-2933				
		2x Additional Processor Selected				
		4 CPU Heatsink, 3UPI				
		iDRAC Group Manager, Enabled 				
		iDRAC,Factory Generated Password				
		Ex-factory Delivery				
		2.5" Chassis with up to 24 Hard Drives 				
		PowerEdge R940 Shipping - APCC/TW V2 				
		14G OpenManage placemat				
		PowerEdge R940 Shipping Material				
		Dell/EMC LCD Bezel for PowerEdge R940				
		Dell EMC Luggage Tag				
		Quick Sync 2 (At-the-box mgmt) 				
		3200MT/s RDIMMs				
		Performance Optimized 				
		64GB RDIMM, 3200MT/s, Dual Rank				
		iDRAC9,Enterprise				
		2x 800GB SSD SAS Mix Use 12Gbps 512e 2.5in Hot-plug AG Drive, 3 DWPD, 4380TBW				
		1.2TB 10K RPM SAS 12Gbps 512n 2.5in Hot-plug Hard Drive				
		PERC H740P RAID Controller, 8GB NV Cache, Adapter, Full Height				
		Dell EMC PowerEdge SFP+ SR Optic 10GbE 850nm 				
		Performance BIOS Settings				
		Dual, Hot-plug, Redundant Power Supply (1+1), 1600W, 250 Volt Power Cord Required for Use				
		Jumper Cord - C13/C14, 2M, 250V, 10A (EU, TW, APCC countries except ANZ)				
		PowerEdge R940 CE Marking for over 1300W PSU, APCC/Taiwan 				
		Broadcom 5720 Quad Port 1GbE BASE-T, rNDC				
		Intel X710 Quad Port 10GbE Direct Attach SFP+ Adapter, PCIe Full Height				
		Emulex LPE 31002 Dual Port 16Gb Fibre Channel HBA, PCIe Full Height				
		Cable Management Arm				
		Unconfigured RAID				
		UEFI BIOS Boot Mode with GPT Partition 				
		ProSupport Plus and Next Business Day Onsite Service-ACDTS Initial, 36Month(s)				
		Mod Specs Info (SADMG)				
		EX-HUB from APCC Penang(DDD) 				
3	Máy chủ Ứng dụng AI		Chiếc	2	"PowerEdge C4140 Server
Hãng sản xuất: Dell - Mỹ"	Chạy ứng dụng Artificial Intelligence and MC
		PowerEdge C4140 Server				
		2 x Intel Xeon Gold Scalable 6148 processor				
		384GB DDR4 @ 2,667 MHz				
		2 x SSDR, 120G, SATA, M.2, IN, BOSS				
		4 x NVIDIA Tesla P100 SXM2				
4	Hệ thống lưu trữ SAN		Chiếc	1	"Mã sản phẩm: ME4024
Hãng sản xuất: Dell - Mỹ"	
		2U Rack 				
		- 02 * Bộ điều khiển chạy song song cho phép thay thế nóng. Trên mỗi bộ điều khiển sử dụng:				
		+ Bộ nhớ cho mỗi bộ điều khiển: 8GB. Tổng 16GB				
		+ Bộ vi xử lý: Intel Broadwell-DE Processor 2 cores 2.2GHz				
		- FC (16Gbps), iSCSI (10Gbps và 1 Gbps), SAS (12Gbps)				
		- Hỗ trợ đồng thời nhiều loại kết nối				
		- Hỗ trợ lên đến 8 cổng kết nối				
		"20 ổ x 2,4TB Sas 10krpm
10 ổ x 1.2TB Sas 10krpm"				
		2/276				
		3PB (with ME484 expansion)				
		Microsoft Windows 2016 and 2012 R2, RHEL 6.9 and 7.4, SLES 12.3, VMware 6.5 and 6.0, Microsoft Hyper-V				
		Hỗ trợ các chức năng sau:				
		- Auto-Tiering: hỗ trợ lên đến 3 primary tiers				
		- hỗ trợ Raid 0,1,5,6,10,50 hoặc Distributed Raid (ADAPT). Cho phép kết hợp bất kỳ loại Raid nào trên cùng một Single Array				
		- Chức năng Thin Provisioning: Kích hoạt (active) mặt định trên toàn bộ các volumes				
		- Chụp ảnh dữ liệu (Snapshots): hỗ trợ lên tối đa 1024 bản chụp trên một array				
		Hỗ trợ các ổ all-flash, ổ lai hybrid hoặc toàn bộ ổ HDD arrays:				
		- NLSAS 7.2K 3.5” – 4TB, 8TB, 12TB, 12TB SED				
		• NLSAS 7.2K 2.5” – 2TB, 2TB SED				
		• SAS 10K 2.5” – 1.2TB, 1.8TB, 2.4TB, 2.4TB SED				
		• SAS 15K 2.5” – 900GB, 900GB SED				
		• SSD – 480GB, 960GB, 1.92TB, 1.92TB SED				
		• SDD and HDD: FIPS-certified SEDs				
		Hỗ trợ các loại khay đĩa mở rộng sau:				
		12 x 3.5” drive bays (12Gb SAS)				
		24 x 2.5” drive bays (12Gb SAS)				
		84 x 3.5” drive bays (12Gb SAS)				
		- Hỗ trợ ổ tự mã hóa loại  SSD hoặc HDD				
		- Hỗ trợ thuật toán mã hóa dựa trên AES-256				
		-  Hổ trợ chuẩn FIPS 140-2 Level 2 				
		hỗ trợ đồng bộ với các tủ đĩa khác cùng loại				
		đồng bộ thông qua giao thức FC hoặc iSCSI				
		cơ chế đồng bộ theo kiểu một - đồng bộ tới nhiều hoặc nhiều thiết bị đồng bộ tới một thiết bị.				
		hỗ trợ các chức năng:				
		- Vmware Site Recovery Manager				
		- mã hoá dữ liệu với các loại ổ SEDs SSD hoặc HDD				
		- ổ mã hoá theo chuẩn FIPS 140-2 Level 2				
5	 SAN Switch		Chiếc	2	" Mã sản phẩm: 300 Brocade
Hãng sản xuất: Dell - Mỹ"	Ghi chú: 8Gb FC Switch 24 x 8G SFP
		8Gb FC Switch (24 x 8G SFP)				
		Fibre Channel Switch				
		24 ports, universal (E, F, M, FL or N)				
		16 ports				
		8 Gbps				
		16 x 8Gbps SW SFP Transceiver				
		16 x 5M, Multi-Mode, LC-LC, Fibre Cable				
		Full fabric architecture with 239 switches maximum				
		1U				
		85 to 264 VAC, Universal 				
6	Hệ điều hành máy chủ: Window 2016 Server Std		Licensse	6	"Hãng sản xuất: Microsoft
Xuất xứ: Singapore"	
7	Hệ quản trị CSDL: SQLSvrStd 2017 SNGL OLP NL		Licensse	2	"Hãng sản xuất: Microsoft
Xuất xứ: Singapore"	
8	Licence ảo hoá máy chủ Vmware		Licensse	4	"Model: VMware vSphere Enterprise Plus 6.5
Hãng sản xuất: Vmware
Xuất xứ: Mỹ"	Thêm, rất quan trọng
9	License AP WLC Wireless Controller 5520		Licensse	50	"Hãng sản xuất: Cisco
Xuất xứ: Mỹ"	Đã có nhưng thiếu khi bổ sung thêm 50 AP
IV	HỆ THỐNG THIẾT BỊ MẠNG, BẢO MẬT					
1	Thiết bị cân bằng tải		Chiếc	2	 FortiADC-1200F - Hãng sản xuất Fortinet - Trung Quốc	
	Loại thiết bị	Application Delivery Controller				
	Phần cứng					
	Kiểu dáng	1RU				
	Giao diện mạng	"Tối thiểu: 8x RJ45 GE port, 8x SFP GE port, 8x SFP+
10G Ports"				
	Cổng quản trị	2 x 10/100/1000 Management Interface				
	Bộ nhớ	32 GB				
	Ổ cứng có sẵn kèm theo thiết bị	240 GB SSD				
	Giao diện quản trị	HTTPS, SSH CLI, Direct Console DB9 CLI, SNMP				
	Nguồn cấp	Dual, 100-240V AC, 50–60 Hz				
	Hiệu năng thiết bị					
	Thông lượng lớp 4	40 Gbp				
	Thông lượng lớp 7	30 Gbps				
	Layer 4 CPS	1,000,000				
	Số phiên L4 đồng thời tối đa	36M				
	L4 HTTP RPS	3,000,000				
	SSL CPS/TPS 2048 Key	35,000				
	SSL Bulk Encryption Throughput	20 Gbps				
	Thông lượng nén 	20 Gbps				
	Khả năng ảo hóa	45				
	Tính năng					
	Tính năng mạng (Network)	- Hỗ trợ IPv4/ IPv6				
		- Hỗ trợ vlan trunking				
		- Hỗ trợ BGP, OSPF with Route Health Inspection (RHI)				
	Cân bằng tải lớp 4	- Thuật toán cân bằng tải:Round robin, weighted round robin, least connections, shortest response				
		- Hỗ trợ các cơ chế giữ phiên làm việc: Persistent IP, hash IP/port, hash header, persistent cookie, hash cookie, destination IP hash, URI hash, full URI hash, host hash, host domain hash				
	Cân bằng tải lớp 7	- Hỗ trợ các giao thức: HTTP, HTTPS, HTTP 2.0 GW, FTP, SIP, RDP, RADIUS, MySQL, RTMP, RTSP				
		- Hỗ trợ URL redirect, HTTP request/response rewrite				
		- Hỗ trợ 403 Forbidden Rewrite				
	Chuyển mạch nội dung lớp 7 dựa trên	- HTTP Host, HTTP Request URL, HTTP Referrer				
		- Source IP Address				
	Cân bằng tải đường truyền	- Hỗ trợ Link load banlancing cả hai chiều: inbound/outbound				
		- Hỗ trợ Policy route và source nat				
		- Hỗ trợ Multiple health check target				
		- Hỗ trợ cấu hình các thông số: intervals, retries và timeouts				
		- Hỗ trợ Tunnel routing				
	Global Server Load Balancing	- Hỗ trợ DNSSEC, DNS Access Control Lists				
		- Hỗ trợ cân bằng tải cho các miền sử dụng SSL VPN				
	Tính năng tăng tốc ứng dụng	- Hỗ trợ tính năng giảm tải và tăng tốc SSL (SSL Offloading and Acceleration)				
		- Hỗ trợ các tính năng tăng tốc TCP:				
		 + Connection pooling and multiplexing				
		 + TCP buffering				
		 + HTTP Compression				
		 + HTTP Caching				
		 + QoS				
	Tính sẵn sàng cao	- Active/Passive Failover				
		- Active/Active Failover				
	Các tính năng quản trị	- Single point of cluster management				
		- Hỗ trợ giao diện dòng lệnh (CLI) cho cả cấu hình và giám sát				
		- Hỗ trợ quản lý từ xa qua kết nối bảo mật SSH				
		- Hỗ trợ quản trị bảo mật qua giao diện Web				
		- Hỗ trợ SNMP with private MIBs				
		- Hỗ trợ syslog				
		- Role-based administration				
		- Tích hợp sẵn các công cụ gỡ rối				
		- Real-time monitoring graphs				
		- Tạo báo cáo				
		- RESTful API				
	Bảo hành và hỗ trợ kỹ thuật	12 tháng				
2	Thiết bị chuyển mạch phân phối		Chiếc	6	"Cisco Catalyst 9300 Series Switches:
- Mã sản phẩm : C9300-24T-A
- Hãng sản xuất: Cisco  Mỹ"	"- 03 cho hệ thống server.
- 03 cho hệ thống các toà 315H1, H3, Thí nghiệm."
	Giao diện	24 cổng 10/100/1000 Copper Ports				
	Mô đu hỗ trợ	Thiết bị hỗ trợ Sẵn module  8 x 10GE SFP+				
	Kiến trúc	1RU				
	Hiệu suất hoạt động	"Tốc độ chuyển mạch tối đa : 208 Gbps 
Forwarding Rate : 154.76 Mpps
Stacking bandwidth : 480 Gbps
Bộ nhớ DRAM : 8 GB, Flash: 16 GB
Số địa chỉ MAC hỗ trợ : 32000
Hỗ trợ số VLAN IDs : 4000"				
	"Hỗ trợ Cổng ảo SVI
(Total Switched Virtual Interfaces (SVIs)"	1000				
	Hỗ trợ Jumbo Frame	9198 bytes				
	Hỗ trợ tính năng Stacking 	Hỗ trợ công nghệ StackWise-480 Tecnology hỗ trợ lên đến 480 Gbps				
	Hỗ trợ khả năng High availability	"với các công nghệ:
- Cross-stack EtherChannel
- các giao thức  (MSTP), (PVRST+), 
- Các giao thức khác Nonstop Forwarding (NSF), Graceful Insertion and Removal (GIR), Fast Software Upgrade (FSU), Software Patching (CLI Based)"				
	Hỗ trợ các công nghệ mới	Flexible NetFlow (FNF), NBAR2, Superior QoS, Multicast DNS (mDNS) gateway				
	Giao thức lớp 3 hỗ trợ	"- Các giao thức cơ bản như Layer 2, Routed Access (RIP, EIGRP Stub, OSPF - 1000 routes), PBR, PIM Stub Multicast (1000 routes)), PVLAN, VRRP, PBR, CDP, QoS, FHS, 802.1X, MACsec-128, CoPP, SXP, IP SLA Responder, SSO
- Các giao thức nâng cao như BGP, EIGRP, HSRP, IS-IS, BSR, MSDP, PIM-BIDIR, IP SLA, OSPF
- VRF, VXLAN, TrustSec, SGT, MPLS, mVPN"				
	Nguồn cung cấp	nguồn 350 WAC				
3	Thiết bị chuyển mạch truy cập		Chiếc	15	"Cisco Catalyst 2960-S Series
- Mã sản phẩm: WS-C2960X-48TS-L
- Hãng sản xuất: Cisco  Mỹ"	"- Bổ sung cho Giảng đường H1, H2, H3 (chưa có)
- Thay thế cho nhà Thí Nghiệm đang dùng từ 2006"
	Giao diện Uplink	4 x 1G SFP				
	Số cổng Gigabit	48 x Ethernet 10/100/1000 Gigabit ports				
	Số lượng xếp chồng tối đa	8				
	Băng thông stack	80G				
	Băng thông chuyển tiếp	108Gbps				
	Băng thông chuyển mạch	216Gbps				
	RAM	512MB				
	Bộ nhớ Flash	128MB				
	Bảo hành	12 tháng				
4	Tường lửa trung tâm		Chiếc	2	"Cisco Firepower 4100 Series: 
- Mã sản phẩm : Cisco FPR4112-NGFW-K9
- Hãng sản xuất: Cisco  Mỹ"	Bảo vệ trực tiếp cho hệ thống Server, chạy HA
	Model	FPR4112-NGFW-K9				
	Form factor	1 RU - Fits a standard 19-in. (48.3cm) square-hole rack				
	Rack mount	Slide rails, mount ears, and screws included (4-post EIA-310-D rack)				
	Airflow	Front to rear - Cold aisle to hot aisle				
	Processor	One 12-core 2.1-GHz Intel Xeon 4116				
	Memory	96-GB DRAM - 6 x 16-GB DDR4-2400				
	Maximum number of interfaces	24 With two 8-port network modules installed				
	Management port	One Gigabit Ethernet Supports 1-Gb fiber or copper SFPs				
	Serial port	One RJ-45 console				
	USB port	One USB 2.0 Type A				
	Network ports	Eight fixed 1-Gb and 10-Gb SFP ports (named Ethernet 1/1 through 1/8)				
	SFP ports	Eight fixed 1-Gb and 10-Gb SFP ports				
	Pullout asset card	Displays the serial number; on the front panel				
	Grounding lug	On rear panel				
	Locator beacon	On front panel				
	Power switch	On rear panel				
	Network modules	Two network module slots (network module 2 and network module 3)				
	Supported network modules	8-port 10-Gigabit Ethernet SFP+				
		4-port 40-Gigabit Ethernet QSFP+				
		8-port 1-Gigabit Ethernet copper with hardware bypass				
		2-port 40-Gigabit Ethernet QSFP+ (built-in) with hardware bypass				
		6-port 1-Gigabit Ethernet SX fiber SFP+ (built-in) with hardware bypass				
		6-port 10-Gigabit Ethernet SR fiber SFP+ (built-in) with hardware bypass				
		6-port 10-Gigabit Ethernet LR fiber SFP+ (built-in) with hardware bypass				
	AC power supply	Two (1+1) power supply module slots				
		Ships with one 1100-W AC power supply module				
		Hot-swappable				
	DC power supply (optional)	Two (1+1) power supply module slots				
		950-W DC power supply module				
		Hot-swappable				
	Redundant power	1+1				
	Fan	Six fan module slots				
		5+1 redundancy				
		Hot-swappable				
	Storage	Two SSD slots				
		Ships with one 400-GB SSD installed in slot 1.				
	MSP (optional)	Installed in the second SSD slot only				
	Dimensions (H x W x D)	1.75 x 16.89 x 29.7 in. (4.44 x 42.90 x 75.43 cm)				
		1.75 x 16.89 x 31.52 in. (4.44 x 42.90 x 80.06 cm) with fans				
	Weight	39.4 lb (17.87 kg) two power supply modules, two network modules, six fans				
		31.4 lb (14.24 kg) no power supply modules, no network modules, no fans				
	System power	AC: 100/240 V AC 10 A (at 100 V), 50 to 60 Hz				
		DC: -40 V DC to -60 V DC, 26 A (at -40 V)				
	Temperature	Operating: 32 to 104° F (0 to 40° C) at sea level				
		1° C reduction of maximum for every 1000 ft (305 m) above sea level				
		Nonoperating: -40 to 149°F (-40 to 65° C)				
5	Thiết bị xác thực hệ thống		Chiếc	1	"Secure Network Server 3655
- Hãng sản xuất: Cisco  Mỹ"	Xác thực bảo vệ mọi truy cập từ mạng LAN
	Hỗ trợ các ứng dụng	Identity Services Engine				
	Kích thước Chuẩn Rack	Rack-mountable - 1U				
	Bộ xửa lý	1 – Intel Xeon 2.10 GHz 4116				
	Số lượng Core mỗi CPU	12				
	Bộ nhớ RAM	96 GB (6 x 16 GB)				
	Số lượng Cổng	"2 X 10Gbase-T
4 x 1GBase-T"				
	ổ đữa cứng	"4 - 2.5-in. 
600-GB 6Gb SAS 10K RPM"				
	Số lượng Endpoints hỗ trợ 	25,000				
	Số lượng Endpoints supported per Policy Services Node	"50,000

"				
	Các tính năng của sản phẩm	"Hỗ trợ Identity Service Engine Advantage Subscription - 3 năm cho 2000 License: 
- Basic RADIUS authentication, authorization, and accounting, including 802.1x, MAC Authentication Bypass and Easy Connect, and Web authentication
- MACsec (all)
- SSO, SAML, ODBC–based authentication
- Guest portal and sponsor services
- Representational state transfer (monitoring) APIs
 - External RESTful services (CRUD)-capable APIs
- PassiveID (Cisco Subscribers)
- PassiveID (Non-Cisco Subscribers)
- Secure Wired and Wireless Access
- Device registration (My Devices portal) and provisioning for Bring Your Own Device (BYOD) with built-in Certificate Authority (CA)
- Security Group Tagging (Cisco TrustSec® SGT) and ACI integration
- Basic Asset Visibility and Enforcement (Profiling)
- Basic Asset Feed Service
- Advanced Asset Visibility (Endpoint Analytics)
- Visibility and Enforcement based on Location-based integration
- Context Sharing and Security Ecosystem Integrations
"				
	Nguồn: 	Hỗ trợ 2 nguồn 770W				
6	Thiết bị wifi Indoor cho phòng học		chiếc	50	" Cisco Aironet AP1840I Series: 
- Mã sản phẩm: AIR-AP1840I-E-K9
 "	Cần mua thêm license cho Wifi Controller 5520
	Hỗ trợ chuẩn 802.11ac Wave1 và Wave2	"●  4x4 DL MU-MIMO with four spatial streams
●  MRC
●  802.11ac beamforming
●  20, 40, 80 MHz channels
●  PHY data rates up to 1733 Mbps (80MHz with 5 GHz)
●  Packet aggregation: A-MPDU (Tx/Rx), A-MSDU (Tx/Rx)
●  802.11 DFS
●  CSD support"				
	Hoạt động băng tần	2.4 GHz và 5 GHz				
	Tốc độ dữ liệu	Lên đến 1733 Mbps trên băng tần 5 GHz				
	Tích hợp Anten	" ●  2.4 GHz, peak gain 4 dBi, internal antenna, omnidirectional in azimuth
●  5 GHz, peak gain 5 dBi, internal antenna, omnidirectional in azimuth"				
	Giao diện	"- 1 x 10/100/1000BASE-T autosensing (RJ45) hỗ trợ PoE
- Cổng quảng lý cấu hình RJ45
- USB 2.0"				
	Bộ nhớ hệ thống	"- 1 GB DRAM
- 256 MB flash"				
	Yêu cầu nguồn đầu vào	"●  13.2W at the PSE (12.1W at the PD) with all features enabled except for the USB port
●  17.8W at the PSE (16.0W at the PD) with the USB port enabled"				
	Chuẩn phù hợp	"- IEC 60950-1; EN 60950-1; AS/NZS 60950.1; UL 60950-1; CAN/CSA-C22.2 No. 60950-1;  UL 2043; Class III Equipment
- Chuẩn IEEE: IEEE 802.11 a/b/g/n/ac, EEE 802.11h, 802.11d ;IEEE 802.3af/at; IEEE 802.3ab; IEEE 802.3
- Bảo mật: 802.11i, Wi-Fi Protected Access 3 (WPA3), WPA2, WPA, 802.1X, Advanced Encryption Standards (AES)
- Chuẩn khác: EAP-Transport Layer Security (TLS),  EAP-Tunneled TLS (TTLS), EAP-Flexible Authentication via Secure Tunneling (EAP-FAST), PEAP v1 or EAP-Generic Token Card (GTC),  EAP-Subscriber Identity Module (SIM)"				
	Hỗ trợ tính năng Wireless Controller	Hỗ trợ tính năng Mobility Express để quản trị Access Point				
