Cài đặt JAVA và setup biến môi trường
===========================================================
yum -y update
yum install wget nano -y
yum -y install java-1.8.0-openjdk-devel
java -version

find /usr/lib/jvm/java-1.8* | head -n 3
/usr/lib/jvm/java-1.8.0
/usr/lib/jvm/java-1.8.0-openjdk
/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.282.b08-1.el7_9.x86_64

update-alternatives --config java
nano /etc/environment
JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.282.b08-1.el7_9.x86_64"

nano ~/.bash_profile
JAVA_HOME=/usr/lib/jvm/jre-1.8.0-openjdk-1.8.0.282.b08-1.el7_9.x86_64
PATH=$PATH:$HOME/bin:$JAVA_HOME/bin
source ~/.bash_profile
echo $JAVA_HOME


Cài đặt Apache Tomcat 10
===========================================================

groupadd tomcat
useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
    -s /bin/false: disable shell access
    -g tomcat: gán user mới vào group tomcat
    -d /opt/tomcat: thư mục home của user.


Lên trang chủ tải và giải nén Apache Tomcat chuyển vào thư mục: /opt/tomcat/
chown -hR tomcat:tomcat /opt/tomcat

Tạo file service
nano /etc/systemd/system/tomcat.service
Copy và paste nội dung bên dưới vào file service:
    [Unit]
    Description=Apache Tomcat 10 Servlet Container
    After=syslog.target network.target
    [Service]
    User=tomcat
    Group=tomcat
    Type=forking
    Environment=CATALINA_PID=/opt/tomcat/tomcat.pid
    Environment=CATALINA_HOME=/opt/tomcat
    Environment=CATALINA_BASE=/opt/tomcat
    ExecStart=/opt/tomcat/bin/startup.sh
    ExecStop=/opt/tomcat/bin/shutdown.sh
    Restart=on-failure
    [Install] 
    WantedBy=multi-user.target

Chạy lệnh
systemctl daemon-reload
systemctl start tomcat
systemctl enable tomcat

Tomcat sẽ start và lắng nghe ở cổng 8080 có thể kiểm tra lại bằng lệnh sau:
netstat -plntu
systemctl status tomcat

Bây giờ mở trình duyệt và truy cập http://your_ip_address:8080. 

Cấu hình Tomcat User, chỉnh tomcat-users.xml sử dụng nano:
nano /opt/tomcat/conf/tomcat-users.xml
    <role rolename="manager-gui"/>
    <user username="admin" password="matkhaucuaban" roles="manager-gui,admin-gui"/>

Mặc định manager gui và admin gui chỉ truy cập từ localhost. Do vậy cần chỉnh sửa file context để truy cập từ xa.

Chỉnh sửa file context.xml trong webapps:
nano /opt/tomcat/webapps/manager/META-INF/context.xml
    <!-- <Valve className="org.apache.catalina.valves.RemoteAddrValve"
            allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" /> -->
 
nano /opt/tomcat/webapps/host-manager/META-INF/context.xml
    <!-- <Valve className="org.apache.catalina.valves.RemoteAddrValve"
            allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" /> -->


docker cp ed21a91fedb8:/usr/local/tomcat/webapps.dist/manager/META-INF/context.xml .
docker cp context.xml ed21a91fedb8:/usr/local/tomcat/webapps.dist/manager/META-INF/context.xml

docker cp ed21a91fedb8:/usr/local/tomcat/webapps.dist/host-manager/META-INF/context.xml .
docker cp context.xml ed21a91fedb8:/usr/local/tomcat/webapps.dist/host-manager/META-INF/context.xml

docker cp ed21a91fedb8:/usr/local/tomcat/webapps.dist/host-manager/WEB-INF/manager.xml .
docker cp manager.xml ed21a91fedb8:/usr/local/tomcat/webapps.dist/host-manager/WEB-INF/manager.xml

Cấu hình tường lửa Firewalld và Nat cổng iptables
===========================================================
Trong trường hợp chưa cài đặt tường lửa Firewalld có thể cài đặt nó sử dụng lệnh sau:

yum install firewalld
Bật tường lửa và để nó chạy khi khởi động lại:
systemctl start firewalld
systemctl enable firewalld

Bây giờ bổ sụng port tomcat 8080 vào tường lửa bằng lệnh sau:
firewall-cmd --zone=public --permanent --add-port=8080/tcp
firewall-cmd --reload

có thể kiểm tra lại tất cả các port và service được mở bằng lệnh sau:
firewall-cmd --list-ports
firewall-cmd --list-services

Cách start Tomcat ở cổng 80
Để start Tomcat ở cổng 80 do vậy không còn nhập cổng khi truy cập, chỉ có một cách như sau. Gõ vào 2 lệnh sau. Những lệnh này sẽ forward cổng 80 sang cổng 8080 mà Tomcat đang lắng nghe:
iptables -t nat -A PREROUTING -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 8080
iptables -t nat -A PREROUTING -p udp -m udp --dport 80 -j REDIRECT --to-ports 8080

Note: Cách thay đổi file server.xml từ cổng 8080 sang 80 sẽ không làm việc vì chúng ta đang start Tomcat không phải user root. Những use không phải là root sẽ không có quyền mở cổng.



Các câu lệnh về Docker
===========================================================
docker search --limit 20 [image-name]

docker pull [image-name]
docker images

docker run --name tomcat-container -p 8080:8080 tomcat:latest
docker run -d --name tomcat-container -p 8080:8080 tomcat:latest
docker run -rm --name tomcat-container -p 8080:8080 tomcat:latest
docker run -it --name tomcat-container -p 8080:8080 tomcat:latest
docker run -it --name tomcat-container -p 8080:8080 tomcat:latest bash # Vào môi trường container
docker run -it -d -p 8080:8080 tomcat:latest bash

docker ps
docker ps -a

docker stop container-id
docker rm container-id
# Xoá không cần stop
docker rm -f container-id 

Cách 1
=============================================
sudo docker run \
  --name tomcat \
  -it \
  -p 8080:8080 \
  -v /tmp/tomcat-users.xml:/usr/local/tomcat/conf/tomcat-users.xml \
  -v /tmp/context.xml:/tmp/context.xml \
  tomcat:latest \
  bash -c "mv /usr/local/tomcat/webapps /usr/local/tomcat/webapps2; cp -f /tmp/context.xml /usr/local/tomcat/webapps.dist/manager/META-INF/context.xml; mv /usr/local/tomcat/webapps.dist /usr/local/tomcat/webapps; catalina.sh run"
=> Loi lenh cp khong co quyen


Cách 2
=============================================
=====> Bước 1
docker run -it -d -p 8082:8080 -v /tmp/tomcat-users.xml:/usr/local/tomcat/conf/tomcat-users.xml -v /tmp/context.xml:/tmp/context.xml tomcat:latest bash -c "mv /usr/local/tomcat/webapps /usr/local/tomcat/webapps2; mv /usr/local/tomcat/webapps.dist /usr/local/tomcat/webapps; catalina.sh run"
=> OK

=====> Bước 2: lấy Container Id
docker ps -a
docker cp /tmp/context.xml 90ec8f6885c1:/usr/local/tomcat/webapps/manager/META-INF/context.xml
docker cp /tmp/context.xml 90ec8f6885c1:/usr/local/tomcat/webapps/host-manager/META-INF/context.xml



docker cp /tmp/context.xml 6900a69415d6:/usr/local/tomcat/webapps/manager/META-INF/context.xml
docker cp /tmp/context.xml 6900a69415d6:/usr/local/tomcat/webapps/host-manager/META-INF/context.xml
docker cp /tmp/context.xml 1585bc432159:/usr/local/tomcat/webapps/manager/META-INF/context.xml
docker cp /tmp/context.xml 1585bc432159:/usr/local/tomcat/webapps/host-manager/META-INF/context.xml
docker cp /tmp/context.xml 0a3b627a7b64:/usr/local/tomcat/webapps/manager/META-INF/context.xml
docker cp /tmp/context.xml 0a3b627a7b64:/usr/local/tomcat/webapps/host-manager/META-INF/context.xml