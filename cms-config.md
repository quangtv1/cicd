find . -type f -exec grep -l '.btn-secondary, .btn-default' {} \;
_______________________________________________
#Sửa màu của Botton

#Dir file css
./theme/classic/style/moodle.css
./theme/boost/style/moodle.css

/* quangtv sua
  color: #212529;
  background-color: #e9ecef;
  border-color: #e9ecef; 
*/
Màu cam:#ffc142

_______________________________________________
Sửa description trang đăng nhập:

Dir Tiếng Anh: /var/www/html/moodle/lang/en
Dir Tiếng Việt: /var/www/html/moodledata/lang/vi

#Edit moodle.php
$string['cookiesenabled'] = 'Cookies must be enabled in your browser';
$string['cookiesenabled'] = 'SINH VIÊN ĐĂNG NHẬP TẠI ĐÂY';

#Edit auth.php
$string['potentialidps'] = 'Log in using your account on:';
$string['potentialidps'] = 'Yêu cầu: Sinh viên bắt buộc phải đăng nhập được vào email Trường cấp có dạng: ten+mssv@nuce.edu.vn';

_______________________________________________
#Xoá cache
php /var/www/html/moodle/admin/cli/purge_caches.php
Site administration > Development > Purge caches

#Hack pass qua mysql
#Pass Admin gốc moddle cms.nuce.edu.vn
admin\$10$bj3XWt4fYuiq3a7wqCbjkezQqq8NFyUf1iWIXoB36CpIVcVXioJ9S

#Pass đổi 123456a@A
admin\$2a$10$29nfqekWEcyatlFuc0/yE.XEp7VZBOLgQXFtHMzcqShGETkhTd3g2

phuongdm@Phuongdm-PC:~$ rclone copy Downloads/backup.png ggdrive:
phuongdm@Phuongdm-PC:~$ rclone ls  ggdrive:/


############### CÀI RCLONE ###############

$ curl https://rclone.org/install.sh | sudo bash
$ rclone config
// Hướng dẫn: https://news.cloud365.vn/cau-hinh-rclone-ket-noi-voi-google-drive
$ rclone copy cms-29-03-2021.sql.zip ggdrive:/BACKUP-SITE/CMS.NUCE.EDU.VN/cms-20210330
$ rclone ls  ggdrive:/BACKUP-SITE/CMS.NUCE.EDU.VN/cms-20210330
$ rclone copy /var/www/html/moodledata ggdrive:/BACKUP-SITE/CMS.NUCE.EDU.VN/cms.nuce.edu.vn-20213003/moodledata


############### CÀI RCLONE ###############

<IfModule mod_userdir.c>
    UserDir disabled
</IfModule>

<Directory "/home/*/public_html">
    AllowOverride FileInfo AuthConfig Limit Indexes
    Options MultiViews Indexes SymLinksIfOwnerMatch IncludesNoExec
    Require method GET POST OPTIONS
</Directory>

#SITE cms.nuce.edu.vn - ATUNG
<VirtualHost *:80>
        ServerAdmin admin@nuce.edu.vn
        DocumentRoot /var/www/html/moodle/
        ServerName cms.nuce.edu.vn
        ServerAlias www.cms.nuce.edu.vn

        <Directory /var/www/html/moodle/>
                Options FollowSymLinks
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        ErrorLog /var/log/httpd/cms.nuce.edu.vn-error_log
        CustomLog /var/log/httpd/cms.nuce.edu.vn-access_log common
</VirtualHost>


# Xử lý sự cố ngày 12-7-2021

# Just an FYI, this is why: https://jira.mariadb.org/browse/MDEV-23497
# tldr, fix:
# innodb_read_only_compressed=OFF in mysql.cnf
# or
# SET GLOBAL innodb_read_only_compressed=OFF;


mysqldump -u root -p cms > cms-13-7-2021.sql 

# Xem size của từng DB
SELECT table_schema "DB Name", Round(Sum(data_length + index_length) / 1024 / 1024, 1) "DB Size in MB"
FROM information_schema.tables
GROUP BY table_schema;

# Xem size của bảng
SELECT table_schema as `Database`,table_name AS `Table`,round(((data_length + index_length) / 1024 / 1024), 2) `Size in MB`
FROM information_schema.TABLES
ORDER BY (data_length + index_length) DESC
LIMIT 5; 

# Bảng nhiều nhất
mdl_logstore_standard_log

# Xem bật inno trên bảng chưa
show variables like "innodb_file_per_table";
# Tối ưu => A Tùng đang xem flush
OPTIMIZE table mdl_logstore_standard_log;
