>G -- Tab lùi dòng
A, $a -- Về cuối dòng chuyển mode Insert
. -- Thực hiện lặp lại
C, c$ -- Xoá từ trỏ đến cuối dòng
s, cl -- Xoá một ký tự ngay trỏ
S, ^C -- 
0, ^ -- Về đầu dòng
I, ^i -- Thêm đầu dòng
A, $a -- Thêm cuối dòng
o, O -- Thêm một dòng trên, dưới
f{char} -- Nhảy đến ký tự {char}
; -- Lặp lại tìm kiếm, giống F3
/chu_can_tim<CR> => n next, N ngược lại
?chu_can_tim<CR> => n next, N ngược lại
:s/chu_can_thay/chu_thay/g -- Thay thế toàn bộ
* -- Hightlight các từ giống từ tại con trỏ, nếu có con trỏ nhảy sang từ tiếp theo
db -- Xoá các ký tự của từ trước con trỏ (b,w: đầu từ)
x -- Xoá một ký tự
daw -- Xoá một từ
dbx,bdw,daw -- Xoá một từ
dap -- Xoá một đoạn
d2w, 2dw, c2w -- Xoá nhiều từ
<C-a>, <C-x> -- Cộng trừ thêm 1 => 10<C-a>: cộng thêm 10

c -- Thay đổi
d -- Xoá
y -- Yank into register


CÁC CÂU LỆNH DI CHUYỂN
----------------------------------
H -- Đi qua TRÁI
J -- Đi XUỐNG
K -- Đi LÊN
L -- Đi qua PHẢI
CTRL + u -- Giống Page up
CTRL + d -- Giống Page down
b -- Nhẩy về từ trước
w -- Nhẩy về từ tiếp
e -- Nhẩy cuối từ hiện tại
$ -- Nhảy về CUỐI dòng hiện tại => :$ cuối file
0 -- Nhảy về ĐẦU dòng hiện tại => :0 đầu file
gg -- Nhảy lên đầu file
G -- Nhảy xuống cuối file
20G -- Nhảy tới dòng thứ 20
:set number -- Hiển thị số dòng
CTRL + G -- Hiển thị thông tin dòng hiện tại, tổng số dòng...

v -- Bôi đen vùng text kết hợp với H/J/K/L
V -- Bôi đen nhanh dòng hiện tại
ggVG -- Bôi đen tất giống CTRL + A


CÂU LỆNH CHUNG
----------------------------------
i -- Bật chế độ Insert tại con trỏ
a -- Bật chế độ Insert tại cột tiếp (sau trỏ)
o -- Thêm dòng trắng bên DƯỚI trỏ
O -- Thêm dòng trắng bên TRÊN trỏ
u -- CTRL + Z
CTRL + r -- Chưa hiểu
R -- Bật chế độ Thay thế


:!cau_lenh_terminal -- Chạy câu lệnh từ terminal trong VIM
:q -- Thoát
:q! -- Thoát bất cần
:w -- Lưu
:w! -- Lưu ghi đè
:wq -- Lưu rồi thoát, lệnh khác ZZ


CÂU LỆNH CHỈNH SỬA
----------------------------------
x -- Cắt 01 ký tự tại trỏ
dw -- ?
D -- Cắt từ vị trí con trỏ tới cuối dòng
dd -- Cắt dòng hiện tại
dG -- Cắt đến cuối file

yw -- Copy từ con trỏ đến cuối từ
yy -- Copy cả dòng

p -- Dán sau con trỏ
P -- Dán trước con trỏ
. -- Lặp lại câu lệnh vừa làm 01 lần
5. -- Lặp lại 5 lần ở Nomarl Mode

/noi-dung -- Tìm từ trỏ đến CUỐI
?noi-dung -- Tìm từ trỏ lên ĐẦU
n -- Tiếp tục tìm kiếm giống F3

:s/tu_tim_kiem/tu_thay_the -- Thay thế từ đầu tiên
:%s/tu_tim_kiem/tu_thay_the -- Thay thế TẤT CẢ 'tu_tim_kiem' bằng 'tu_thay_the'
